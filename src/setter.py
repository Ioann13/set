class Setter():
    """Класс для работы со строкой из множества слов"""
    def __init__(self, filepath):
        """Открыть filepath на чтение, считать и сохранить первую строку.
        Дальше весь класс работает с этой считанной строкой.
        Строка - набор слов, разделённых пробельными символами.
        Args:
            filepath - путь к файлу
        Raises:
            ValueError - если первая строка содержит только пробельные символы
            RuntimeError - если filepath - каталог или нет прав
        Side effects:
            Создаёт пустой файл filepath, если его не было

        для примера в файле такая первая строка
        мы   короли ночной вероны мыы короли
        """
        self.data=""
        self.spisok=[]
        try:
            with open(filepath, "r", encoding="utf-8") as file_:
                self.data=file_.readlines(1)
                self.spisok = "".join(self.data).split()
            if self.data.count(' ') == len(self.data):
                    raise ValueError
        except (IsADirectoryError, PermissionError) as err:
            raise RuntimeError(err)


    def __len__(self):
        """
        Returns: число слов в считанной строке

        Для нашего примера вернёт 6
        """
        return len("".join(self.data).split())




    def __iter__(self):
        """
        Итератор по всем словам считанной строки

        Для нашего примера итератор вернёт (за 6 обращений)
        мы
        короли
        ночной
        вероны
        мыы
        короли
        """
        for i in range(len(self.spisok)):
            yield(self.spisok[i])


    def shout(self, count):
        """
        Функция-генератор, итерируется по всем словам
        Слова длиной меньше или равными count нужно выводить ЗАГЛАВНЫМИ буквами
        Args:
            count - длина строки, по умолчанию 3
        Raises:
            ValueError - если count меньше или равно нулю
            RuntimeError - если count не число

        Для нашего примера и count=3 вернёт за 6 обращений
        МЫ
        короли
        ночной
        вероны
        МЫЫ
        короли
        """
        for i in range(len(self.spisok)):
            if count <= 0:
                raise ValueError
            if str(count).isnumeric() == False:
                raise RuntimeError
            if len(self.spisok[i]) <= count:
                yield self.spisok[i].upper()
            else:
                yield self.spisok[i]



    def only_one(self):
        """
        Returns:
            словарь, где ключ - это уникальные слова из считанной строки,
            значения - число символов в этом слове

        Для нашего примера вернёт
        {'мы': 2, 'ночной': 6, 'короли': 6, 'вероны': 6, 'мыы': 3}
        """
        a={}
        for i in range(len(self.spisok)):
            a.update({f"{self.spisok[i]}": len(self.spisok[i])})
        return a